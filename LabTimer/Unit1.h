//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
//---------------------------------------------------------------------------
class TFM : public TForm
{
__published:	// IDE-managed Components
	TButton *Sec;
	TButton *Pause;
	TLabel *Numbers;
	TTimer *Timer;
	TButton *Reebot;
	TEdit *TimeVvod;
	TLabel *TimeLabel;
	void __fastcall ReebotClick(TObject *Sender);
	void __fastcall PauseClick(TObject *Sender);
	void __fastcall SecClick(TObject *Sender);
	void __fastcall TimerTimer(TObject *Sender);
private:	// User declarations
double FTimeValue;
bool FTimePause;
public:		// User declarations
	__fastcall TFM(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFM *FM;
//---------------------------------------------------------------------------
#endif
