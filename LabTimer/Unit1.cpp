//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TFM *FM;
//---------------------------------------------------------------------------
__fastcall TFM::TFM(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFM::ReebotClick(TObject *Sender)
{
	  if (TimeVvod->Text != "") {
	  FTimeValue = Time().Val + TimeVvod->Text.ToDouble()/(24*60*60);
	  FTimePause = false;
	  Timer->Enabled = true;
	  Pause->Text = L"Pause";
}
}
//---------------------------------------------------------------------------
void __fastcall TFM::PauseClick(TObject *Sender)
{
	if (FTimePause)  {
	 FTimeValue = Time().Val + FTimeValue;
	 FTimePause = false;
	 Timer->Enabled = true;
	 Pause->Text = L"Pause";
	}
	else
	{
		Timer->Enabled = false;
		FTimePause = true;
		FTimeValue = FTimeValue - Time().Val;
		Pause->Text = L"Go";
    }
}
//---------------------------------------------------------------------------
void __fastcall TFM::SecClick(TObject *Sender)
{
	FTimeValue = FTimeValue - (double)10/(24*60*60);
}
//---------------------------------------------------------------------------
void __fastcall TFM::TimerTimer(TObject *Sender)
{
	double x = FTimeValue - Time().Val;
	if (x <= 0)
	{
	x = 0;
	Timer->Enabled = false;

	}
	Numbers->Text = FormatDateTime("nn:ss", x);
}
//---------------------------------------------------------------------------
