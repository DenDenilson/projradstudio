//---------------------------------------------------------------------------

#ifndef labNotesDBH
#define labNotesDBH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TStyleBook *StyleBook1;
	TTabControl *TabControl1;
	TTabItem *TabItem1;
	TTabItem *TabItem2;
	TToolBar *ToolBar1;
	TLabel *Label1;
	TButton *Button1;
	TButton *Add;
	TListView *ListView1;
	TToolBar *ToolBar2;
	TLabel *Label2;
	TButton *Button2;
	TLayout *Layout1;
	TLayout *Layout3;
	TLabel *Label3;
	TLabel *Label4;
	TTrackBar *TrackBar1;
	TLabel *Label5;
	TEdit *Edit1;
	TMemo *Memo1;
	TButton *Save;
	TButton *Button3;
	TButton *Button4;
	TLayout *Layout2;
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
