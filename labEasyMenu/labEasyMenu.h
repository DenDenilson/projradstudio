//---------------------------------------------------------------------------

#ifndef labEasyMenuH
#define labEasyMenuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Ani.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *Main;
	TTabItem *tabPlay;
	TTabItem *tabNotes;
	TTabItem *tabAbout;
	TGridPanelLayout *GridPanelLayout1;
	TButton *Notes;
	TButton *About;
	TButton *Exit;
	TToolBar *ToolBar1;
	TLabel *Label1;
	TButton *Back;
	TLayout *Layout1;
	TButton *Easy;
	TButton *Normal;
	TButton *Hard;
	TButton *VeryHard;
	TButton *Impossible;
	TToolBar *ToolBar2;
	TLabel *Label2;
	TButton *Back1;
	TToolBar *ToolBar3;
	TLabel *Label3;
	TButton *Back2;
	TImage *Image1;
	TButton *Play;
	TImage *Image2;
	TFloatAnimation *FloatAnimation1;
	TImage *Image3;
	TFloatAnimation *FloatAnimation2;
	TImage *Image4;
	TFloatAnimation *FloatAnimation3;
	TImage *Image5;
	TFloatAnimation *FloatAnimation4;
	TImage *Image6;
	TFloatAnimation *FloatAnimation5;
	TImage *Image7;
	TFloatAnimation *FloatAnimation6;
	TImage *Image8;
	TFloatAnimation *FloatAnimation7;
	TImage *Image9;
	TFloatAnimation *FloatAnimation8;
	TStyleBook *StyleBook1;
	void __fastcall PlayClick(TObject *Sender);
	void __fastcall NotesClick(TObject *Sender);
	void __fastcall AboutClick(TObject *Sender);
	void __fastcall ExitClick(TObject *Sender);
	void __fastcall BackClick(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
