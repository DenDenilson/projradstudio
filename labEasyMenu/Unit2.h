//---------------------------------------------------------------------------

#ifndef Unit2H
#define Unit2H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TfmuAbout : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar3;
	TLabel *Label3;
	TButton *Back;
	TImage *Image1;
	void __fastcall BackClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TfmuAbout(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfmuAbout *fmuAbout;
//---------------------------------------------------------------------------
#endif
