//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TfmuAbout *fmuAbout;
//---------------------------------------------------------------------------
__fastcall TfmuAbout::TfmuAbout(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TfmuAbout::BackClick(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------
