//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "labEasyMenu.h"
#include "Project1PCH1.h"
#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PlayClick(TObject *Sender)
{
	tc->ActiveTab = tabPlay;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::NotesClick(TObject *Sender)
{
	tc->ActiveTab = tabNotes;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::AboutClick(TObject *Sender)
{
	fmuAbout->ShowModal();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ExitClick(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void __fastcall TForm1::BackClick(TObject *Sender)
{
	tc->ActiveTab = Main;
}

//---------------------------------------------------------------------------
void __fastcall TForm1::FormKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	if ((tc->ActiveTab != Main) & (Key = vkHardwareBack)){
	tc->ActiveTab = Main;
	Key = 0;

	}
}
//---------------------------------------------------------------------------
