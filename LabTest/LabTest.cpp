//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "LabTest.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TFM *FM;
//---------------------------------------------------------------------------
__fastcall TFM::TFM(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFM::ButtonAllClick(TObject *Sender)
{
  Itog->Lines->Add(
	 L"������ " + TC->ActiveTab->Text + "-"+
	((((TControl *)Sender)->Tag == 1) ? L"�����" : L"�������"));
	 TC->Next();

}
//---------------------------------------------------------------------------
void __fastcall TFM::EndClick(TObject *Sender)
{
 TC->ActiveTab = tiStart;
}
//---------------------------------------------------------------------------
void __fastcall TFM::BeginClick(TObject *Sender)
{
 Itog->Lines->Clear();
 TC->Next();
}
//---------------------------------------------------------------------------
void __fastcall TFM::FormCreate(TObject *Sender)
{
 PB->Max = TC->TabCount-1;
 EndClick(End);
}
//---------------------------------------------------------------------------
void __fastcall TFM::TCChange(TObject *Sender)
{
 PB->Value = TC->ActiveTab->Index;
}
//---------------------------------------------------------------------------
