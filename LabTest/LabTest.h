//---------------------------------------------------------------------------

#ifndef LabTestH
#define LabTestH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class TFM : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TLabel *ToolBar;
	TLabel *TestName;
	TTabControl *TC;
	TTabItem *tiStart;
	TTabItem *TabItem2;
	TTabItem *TabItem3;
	TTabItem *TabItem4;
	TTabItem *TabItem5;
	TButton *Begin;
	TLabel *Q1;
	TButton *Q1V1;
	TButton *Q1V2;
	TButton *Q1V3;
	TButton *Q1V4;
	TLabel *Q2;
	TButton *Q2V4;
	TButton *Q2V1;
	TButton *Q2V2;
	TButton *Q2V3;
	TLabel *Label1;
	TButton *Q3V4;
	TButton *Q3V3;
	TButton *Q3V2;
	TButton *Q3V1;
	TButton *End;
	TProgressBar *PB;
	TMemo *Itog;
	TStyleBook *StyleBook1;
	void __fastcall ButtonAllClick(TObject *Sender);
	void __fastcall EndClick(TObject *Sender);
	void __fastcall BeginClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall TCChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TFM(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFM *FM;
//---------------------------------------------------------------------------
#endif
