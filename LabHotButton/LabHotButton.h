//---------------------------------------------------------------------------

#ifndef LabHotButtonH
#define LabHotButtonH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TFM : public TForm
{
__published:	// IDE-managed Components
	TGridPanelLayout *GridPanelLayout1;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TLayout *Layout1;
	TImage *Image1;
	TLayout *Layout2;
	TImage *Image2;
	TLayout *Layout3;
	TRectangle *Rectangle1;
	TLabel *Label1;
	TRectangle *Rectangle2;
	TFloatAnimation *FloatAnimation1;
private:	// User declarations
public:		// User declarations
	__fastcall TFM(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFM *FM;
//---------------------------------------------------------------------------
#endif
