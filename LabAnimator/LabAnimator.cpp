//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "LabAnimator.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	Group->Position->X = -Group->Width;
	FIO->Position->X = this->Width + Group->Width;
	Pres->Position->X = -Pres->Width;
	TAnimator::AnimateIntWait(Group, "Position.X", 0,1, TAnimationType::Out, TInterpolationType::Back);
	TAnimator::AnimateIntWait(FIO, "Position.X", 0,1, TAnimationType::Out, TInterpolationType::Back);
	TAnimator::AnimateIntWait(Pres, "Position.X", 0,1, TAnimationType::Out, TInterpolationType::Back);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
	Yehoo->AutoSize = true;
	Yehoo->TextSettings->Font->Size = 400;
	TAnimator::AnimateIntWait(Yehoo, "TextSettings.Font.Size", 96,1,TAnimationType::Out, TInterpolationType::Linear);
    Yehoo->AutoSize = false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
	int y = Layout1->Position->Y;
	Layout1->Position->Y = -Layout1->Height;
	TAnimator::AnimateIntWait(Layout1, "Position.y", y,1,TAnimationType::Out, TInterpolationType::Back);
}
//---------------------------------------------------------------------------
