//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Effects.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Types.hpp>
#include <FMX.Filter.Effects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Controls.Presentation.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TImage *Image1;
	TBlurEffect *BlurEffect1;
	TImage *Image2;
	TImage *Image3;
	TImage *Image4;
	TImage *Image5;
	TImage *Image6;
	TGlowEffect *GlowEffect1;
	TBevelEffect *BevelEffect1;
	TReflectionEffect *ReflectionEffect1;
	TRippleEffect *RippleEffect1;
	TMagnifyEffect *MagnifyEffect1;
	TSwirlEffect *SwirlEffect1;
	TSmoothMagnifyEffect *SmoothMagnifyEffect1;
	TBandsEffect *BandsEffect1;
	TButton *Button1;
	TButton *Button2;
	TDropTransitionEffect *DropTransitionEffect1;
	TRotateCrumpleTransitionEffect *RotateCrumpleTransitionEffect1;
	TWaterTransitionEffect *WaterTransitionEffect1;
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
